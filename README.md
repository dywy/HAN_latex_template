# HAN latex template
Sharing Latex templates for HAN University of Applied Sciences.
Nothing special realy, just a place to maintain and share latex templates to replace MS word.
You may like it, you may not. There's always room for improvement. But always be nice.
## Instructions
Copy all files and start from the `.tex` file.
I like to use a seperate build directory, so thats why that exists. You may need change your prefered editor to do so also, or change it to whatever your prefence is.
The `code` and `images` folders hold contents refered to by the `.tex` file.
The `references.bib` file is where I like to put, well, references. Both Mendeley and Zotero generate good `.bib` files. I have not used other reference managers. The Zotero client is opensource and runs with fewer issues than Mendeley on Linux.

## Texmaker note
Assuming _use a "build" subdirectory for ouput files_ is set, use the following command for Makeindex: 'makeindex build/%.nlo -s nomencl.ist -o build/%.nls'.

Also, to get biblatex to use biber instead of bibtex, I have 'biber build/%.bcf' as the Bib(la)tex command.

## Package notes
These are the texlive (meta) packages installed on my archlinux machine which renders this template without errors:  
`texlive-bibtexextra 2018.50004-1`  
`texlive-bin 2018.48691-9`  
`texlive-core 2018.50036-1`  
`texlive-latexextra 2018.50031-1`  
`texlive-pictures 2018.50020-1`  
`texlive-science 2018.50013-1`  

For debian based distro's, the following are probably the equivelant packages, but this was not tested:  
`texlive-bibtex-extra`  
`texlive-binaries`  
`texlive-base`  
`texlive-latex-extra`  
`texlive-pictures`  
`texlive-science`  

For Windows, you probably want either the MiKTeX, proTeXt or TeX Live distribution; they contain a complete TeX system with LaTeX itself and editors to write documents. Consider the MacTeX distribution when on Mac OS. I've not tested any of these.

You may want to consider online Latex services, which seem to be gaining in poularity. Such include Papeeria, Overleaf, ShareLaTeX, Datazar, and LaTeX base.
